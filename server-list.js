// Define the list of available Electrum Cash servers.
const serverList =
[
	// Imaginary Usernames servers:
	{
		host: 'bch.imaginary.cash',
		version: '1.4.3',
		transports:
		{
			TCP: 50001,
			TCP_TLS: 50002,
			WS:  50003,
			WSS: 50004,
		},
	},
	{
		host: 'electrum.imaginary.cash',
		version: '1.4.3',
		transports:
		{
			TCP: 50001,
			TCP_TLS: 50002,
			WS:  50003,
			WSS: 50004,
		},
	},

	// Georg Engelmann provides electroncash.de
	{
		host: 'electroncash.de',
		version: '1.4.3',
		transports:
		{
			TCP: 50001,
			TCP_TLS: 50002,
			WS:  60001,
			WSS: 60002,
		},
	},
];

// Export the server list.
module.exports = serverList;
