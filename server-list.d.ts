// Define the structure used to provide port numbers for supported transports.
export interface electrumTransports
{
	TCP?: number;
	TCP_TLS?: number;
	WS?: number;
	WSS?: number;
}

// Define the structure used to represent an electrum server.
export interface electrumServer
{
	host: string;
	version: string;
	transports: electrumTransports;
}

// Export server list
declare const serverList: electrumServer[];
export = serverList;
